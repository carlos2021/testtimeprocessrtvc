import os
from utility import *
from dotenv import load_dotenv
load_dotenv()


rutaArchivos = os.getenv("PROCES_FOLDER")
archivos = os.listdir(rutaArchivos)


for archivo in archivos:
    try:
        howManyFrames = preprocesarVideo(archivo,rutaArchivos)
        archivoOriginal = f"{rutaArchivos}/{archivo}"
        cuantoPesa = peso(archivoOriginal)
        cuantoDura = duracion(archivoOriginal)
        f = open ('registroProcesado.txt','a+')
        f.write("\n"+archivo +","+ cuantoPesa+","+ cuantoDura + "," + str(howManyFrames))
        f.close()
        os.system(f"rm '{archivoOriginal}'")
        os.system(f"rm -r '{os.getenv('PROJECT_PATH')}/media/{archivo.replace(' ','-').split('.')[0]}'")
    except:
        print("Fallo al procesar")