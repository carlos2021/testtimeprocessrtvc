import os
import cv2
import json
import subprocess
import subprocess32 as sp
from utility import *
from datetime import datetime
from google.cloud import storage
from dotenv import load_dotenv
load_dotenv()


def cut_frames(input,output):
    min_frames = {}
    video = cv2.VideoCapture(input)
    count = 0
    frames = 0
    bandera = 1
    while bandera:
        bandera,img = video.read()
        if count == 200:
            destine = f"{output}/{frames}.jpg"
            cv2.imwrite(destine,img)
            frames += 1
            min_frames[int((count/100)*frames)*5] = destine
            count = 0
        else:
            count += 1
    return min_frames


def insert_frames(frames,archivo):
    howManyFrames = 0
    for f in frames.keys():
        url=uploadFileBucket(frames[f], archivo)
        howManyFrames = howManyFrames + 1
    return howManyFrames

def createFolderBucket(pathFile, fotogramas=None):
    os.system(f'export GOOGLE_APPLICATION_CREDENTIALS={os.getenv("GOOGLE_APPLICATION_CREDENTIALS")}')
    bucketName = os.getenv("BUCKET_NAME")
    cliente = storage.Client()
    bucket  = cliente.bucket(bucketName)
    fileName = pathFile.split("/")[-1].split(".")[0]
    if fotogramas == None:
        blob = bucket.blob(f"media/{fileName}/")
        blob.upload_from_string(f"newFolder/")
    else:
        blob = bucket.blob(f"media/{fileName}/{fotogramas}/")
        blob.upload_from_string(f"newFolder/")
    uri = f"gs://{bucketName}/media/{fileName}/"
    os.system("unset GOOGLE_APPLICATION_CREDENTIALS")


def uploadFileBucket(pathfile, archivo=None):
    os.system(f'export GOOGLE_APPLICATION_CREDENTIALS={os.getenv("GOOGLE_APPLICATION_CREDENTIALS")}')
    bucketName = os.getenv("BUCKET_NAME")
    cliente = storage.Client()
    bucket  = cliente.bucket(bucketName)
    fileName = pathfile.split("/")[-1]
    file = fileName.split(".")[0]
    if archivo == None: 
        blob = bucket.blob(f"media/{file}/{fileName}")
        blob.upload_from_filename(pathfile)
        blob.make_public()
    else:
        nameArchivo = archivo.split(".")[0]
        blob = bucket.blob(f"media/{nameArchivo}/fotogramas/{fileName}")
        blob.upload_from_filename(pathfile)
        blob.make_public()
    url = blob.public_url
    os.system("unset GOOGLE_APPLICATION_CREDENTIALS")
    return url


def preprocesarVideo(archivo,rutaArchivos):
    timeInicial = datetime.now()
    archivoOriginal = f"{rutaArchivos}/{archivo}"
    os.mkdir(f"{os.getenv('PROJECT_PATH')}/media/{archivo.replace(' ','-').split('.')[0]}")
    os.mkdir(f"{os.getenv('PROJECT_PATH')}/media/{archivo.replace(' ','-').split('.')[0]}/fotogramas")
    archivoOptimizado = f"{os.getenv('PROJECT_PATH')}/media/{archivo.replace(' ','-').split('.')[0]}/{archivo.replace(' ','-').split('.')[0]}.mp4"
    evento = subprocess.Popen(["ffmpeg","-nostdin","-i",archivoOriginal,"-b:v","150k","-bufsize","150k","-r","20","-s","854x480","-vcodec","libx264","-threads","6","-aspect","854:480",archivoOptimizado,"-loglevel","fatal"])
    evento.communicate()
    createFolderBucket(archivoOptimizado)
    createFolderBucket(archivoOptimizado, "fotogramas")
    uploadFileBucket(archivoOptimizado)
    file = archivoOptimizado
    aux = file.split("/")[:-1]
    ruta = "/".join(aux)
    name = file.split("/")[-1].split(".")[:-1][0]
    # a = subprocess.Popen(["ffmpeg","-nostdin","-i",f"{file}","-c:a" ,"flac", "-compression_level", "12" ,"-ac","1","-threads","6","-filter:a",'atempo=0.75',f"{ruta}/{name}.flac","-loglevel","fatal"])
    # a.communicate()
    # rutaFlac = f"{ruta}/{name}.flac"
    # uploadFileBucket(rutaFlac)
    
    relacion_min = cut_frames(archivoOptimizado,f"{os.getenv('PROJECT_PATH')}/media/{archivo.replace(' ','-').split('.')[0]}/fotogramas")
    howManyFrames = insert_frames(relacion_min, archivoOptimizado.split("/")[-1])

    timeFinal = datetime.now()
    print(timeFinal -timeInicial)
    return howManyFrames   


def probe(vid_file_path):
    if type(vid_file_path) != str:
        raise Exception('Gvie ffprobe a full file path of the video')
        return
    command = ["ffprobe",
            "-loglevel",  "quiet",
            "-print_format", "json",
             "-show_format",
             "-show_streams",
             vid_file_path
             ]
    pipe = sp.Popen(command, stdout=sp.PIPE, stderr=sp.STDOUT)
    out, err = pipe.communicate()
    return json.loads(out)


def durationMp4(vid_file_path):
    _json = probe(vid_file_path)
    if 'format' in _json:
        if 'duration' in _json['format']:
            return float(_json['format']['duration'])
    if 'streams' in _json:
        for s in _json['streams']:
            if 'duration' in s:
                return int(s['duration'])


def horaMinSeg(time):
    time = time/3600
    hours = int(time)
    minutes = (time*60) % 60
    seconds = (time*3600) % 60
    duration = ("%02d:%02d:%02d" % (hours, minutes, seconds))
    return duration


def peso(fname):
    sizeInt = (os.stat(fname).st_size/1000000)
    size = str(int(sizeInt))
    return size + " megabytes."


def duracion(fname: str):
    duration="duracion no definida: extension no disponible"
    try:
        time = durationMp4(fname)
        duration = horaMinSeg(time)
    except Exception as Expect: 
        pass
    return duration
